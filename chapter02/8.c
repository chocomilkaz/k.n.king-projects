#include <stdio.h>

int main(void)
{
	float loan;
	float interest;
	float monthly;
	
	printf("Enter amount of loan: ");
	scanf("%f", &loan);
	printf("Enter interest rate: ");
	scanf("%f", &interest);
	printf("monthly payment: ");
	scanf("%f", &monthly);

	float monthly_interest = 1 + interest /100 / 12;
	float remain = loan * monthly_interest - monthly;
	printf("Balance remaining after first payment: $%.2f\n", remain);
	remain = remain * monthly_interest - monthly;
	printf("Balance remaining after second payment: $%.2f\n", remain);
	remain = remain * monthly_interest - monthly;
	printf("Balance remaining after third payment: $%.2f\n", remain);

	return 0;
}
