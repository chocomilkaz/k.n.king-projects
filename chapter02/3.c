#include <stdio.h>
#define VOL_SPHERE (4.0f/3.0f*3.14f)

int main(void)
{
	int r;
	scanf("%d", &r);
	float result = r*r*r*VOL_SPHERE;
	printf("the result is %.2f\n", result);
	return 0;
}
