#include <stdio.h>
#define TAX 1.05f

int main(void)
{
	float user_amount;
	printf("Enter an amount: ");
	scanf("%f", &user_amount);
	float result = user_amount * TAX;
	printf("With tax added: $%.2f\n", result);
	return 0;
}
