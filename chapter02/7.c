#include <stdio.h>

int main(void)
{
	int input;
	printf("Enter U.S. Dollar amount: ");
	scanf("%d", &input);

	int	bill20 = input / 20;
	int remain = input - 20 * bill20;
	int	bill10 = remain / 10;
	remain = remain - 10 * bill10;
	int	bill5 = remain / 5;
	remain = remain - 5 * bill5;

	printf("$20 bills: %d\n", bill20);
	printf("$10 bills: %d\n", bill10);
	printf(" $5 bills: %d\n", bill5);
	printf(" $1 bills: %d\n", remain);

	return 0;

}
