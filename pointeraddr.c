#include <stdio.h>

int main(void)
{
	int a[] = {1,2,3,4,5};
	int *p;
	p = &a[0];
	printf("address of the pointer at a[0] is %d\n", p);
	printf("address of the pointer at a[1] is %d\n", p+1);
	printf("address of the pointer at a[2] is %d\n", p+2);

	return 0;
}
