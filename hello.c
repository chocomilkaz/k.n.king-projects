#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *source_fp, *dest_fp;
	int ch;
	source_fp = fopen(argv[1], "rb");
	dest_fp = fopen(argv[2], "wb");
	while ((ch= getc(source_fp)) != EOF)
		putc(ch, dest_fp);
	
	fclose(source_fp);
	fclose(dest_fp);
	return 0;
}

