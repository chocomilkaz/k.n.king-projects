#include <stdio.h>

int main(void)
{
	int y, m, d, y2, m2, d2;

	printf("Enter first date (mm/dd/yy): ");
	scanf("%d/%d/%d", &m, &d, &y);
	printf("Enter second date (mm/dd/yy): ");
	scanf("%d/%d/%d", &m2, &d2, &y2);

	if (y < y2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m, d, y, m2, d2, y2);
	else if (y > y2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m2, d2, y2, m, d, y);
	else if (m < m2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m, d, y, m2, d2, y2);
	else if (m > m2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m2, d2, y2, m, d, y);
	else if (d < d2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m, d, y, m2, d2, y2);
	else if (d > d2)
		printf("%d/%d/%d is earlier than %d/%d/%d", m2, d2, y2, m, d, y);

	return 0;
}
