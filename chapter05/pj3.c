#include <stdio.h>

int main(void)
{
	float commission, numshare, priceshare, value, rivalfee;

	printf("Number of shares?: ");
	scanf("%f", &numshare);
	printf("Price per share?: $");
	scanf("%f", &priceshare);
	value = numshare * priceshare;

	if (value < 2500.00f)
		commission = 30.00f + .017f * value;
	else if (value < 6250.00f)
		commission = 56.00f + .0066f * value;
	else if (value < 20000.00f)
		commission = 76.00f + .0034f * value;
	else if (value < 50000.00f)
		commission = 100.00f + .0022f * value;
	else if (value < 500000.00f)
		commission = 155.00f + .0011f * value;
	else
		commission = 255.00f + .0009f * value;
	
	if (commission < 39.00f)
		commission = 39.00f;

	if (numshare<2000)
		rivalfee = 33.00f + 0.03f * numshare;
	else 
		rivalfee = 33.00f + 0.02f * numshare;

	printf("Our commission: $%.2f\n", commission);
	printf("Rival's commission: $%.2f\n", rivalfee);
	if(commission < rivalfee)
		printf("Use us!\n");
	else
		printf("Use them!\n");

	return 0;
}
