#include <stdio.h>

int main(void)
{
	int i1, i2, i3, i4, max, min;

	printf("Enter 4 integers: ");
	scanf("%d%d%d%d", &i1, &i2, &i3, &i4);
	
	if ( i1 < i2 && i3 < i4){
		if ( i2 < i4)
			max = i4;
		else
			max = i2;
		if ( i1 < i3)
			min = i1;
		else 
			min = i3;
	}
	
	if ( i1 < i2 && i3 > i4){
		if ( i2 < i3)
			max = i3;
		else
			max = i2;
		if ( i1 < i4)
			min = i1;
		else 
			min = i4;
	}
	
	if ( i1 > i2 && i3 < i4){
		if ( i1 < i4)
			max = i4;
		else
			max = i1;
		if ( i2 < i3)
			min = i2;
		else 
			min = i3;
	}

	if ( i1 > i2 && i3 > i4){
		if ( i1 < i3)
			max = i3;
		else
			max = i1;
		if ( i2 < i4)
			min = i2;
		else 
			min = i4;
	}

	printf("Largest : %d\n", max);
	printf("Smallest : %d\n", min);
	return 0;

}
