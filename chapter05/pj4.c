#include <stdio.h>

int main(void)
{
	int wind;

	printf("Enter wind speed(in knots): ");
	scanf("%d", &wind);

	if(wind < 1)
		printf("The wind is Calm\n");
	else if(wind < 3)
		printf("The wind is Light air\n");
	else if(wind < 27)
		printf("The wind is Breze\n");
	else if(wind < 47)
		printf("The wind is Gale\n");
	else if(wind < 63)
		printf("The wind is Storm\n");
	else 
		printf("The wind is Hurricane\n");

	return 0;
}
