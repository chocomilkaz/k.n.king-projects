#include <stdio.h>

int main(void)
{
	int input;	
	
	printf("Enter a number: ");
	scanf("%d", &input);

	if(input<9)
		printf("Numer has 1 digit.\n");
	else if(input<99)
		printf("Number has 2 digit.\n");
	else if(input<999)
		printf("Number has 3 digit.\n");
	else 
		printf("Number has 4 digit.\n");

	return 0;

}
