#include <stdio.h>

int main(void)
{
	int input;
	printf("Enter a number between 0 and 32767: ");
	scanf("%d", &input);

	int d1, base, d2, d3, d4, d5, d6;
	d1 = input % 8;
	d2 = input /8 % 8;
	d3 = input /8 /8 % 8;
	d4 = input /8 /8 /8 % 8;
	d5 = input /8 /8 /8 /8 % 8;
	d6 = input /8 /8 /8 /8 /8 % 8;
	



	printf("In Octal, your number is: %d%d%d%d%d%d\n", d6, d5, d4, d3, d2, d1);
	return 0;
	
}
