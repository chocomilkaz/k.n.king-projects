
#include <stdio.h>

int main(void)
{
	int large, middle, small;
	printf("Enter a three-digit number: ");
	scanf("%1d%1d%1d", &large, &middle, &small);

	printf("The reversal is: %d%d%d\n", small, middle, large);
	return 0;

	
}
