#include <stdio.h>

int main(void)
{
	int num, large, small;
	printf("Enter a two-digit number: ");
	scanf("%d", &num);

	large = num / 10;
	small = num % 10;

	printf("The reversal is: %d\n", small*10+large);
	return 0;

	
}
