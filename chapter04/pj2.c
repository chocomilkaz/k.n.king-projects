#include <stdio.h>

int main(void)
{
	int num, large, middle, small;
	printf("Enter a three-digit number: ");
	scanf("%d", &num);

	large = num / 100;
	middle = num %100 / 10;
	small = num % 10;

	printf("The reversal is: %d\n", small*100+middle*10+large);
	return 0;

	
}
