#include <stdio.h>

#define N (int)(sizeof(a)/sizeof(a[0]))

int main(void)
{

	int input;
	
	printf("How many numbers do you like to enter?");
	scanf("%d", &input);
	int a[input], i;

	

	for (i=0; i<N; i++){
		printf("input %d", i+1);
		switch(i+1){
			case 1:
				printf("st number: ");
				break;
			case 2:
				printf("nd number: ");
				break;
			case 3:
				printf("rd number: ");
				break;
			default:
				printf("th number: ");
				break;
		}
		scanf("%d", &a[i]);
	}

	printf("In reverse order: ");
	for (i =N -1; i >= 0; i--)
		printf(" %d", a[i]);
	
	printf("\n");

	return 0;

}
