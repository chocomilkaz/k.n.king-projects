#include <stdio.h>

int main(void)
{
	printf("Enter a first and last name: \n");

	int ch, first=0, i;
	int a[20]={' '};

	while((ch=getchar())==' ')
		;

	first = ch;

	while(getchar()!=' ')
		;

	while((ch=getchar())==' ')
		;

	a[0] = ch;

	for(i=1;(ch=getchar())!=' ' && ch !='\n'; i++)
		a[i] = ch;

	for(i=0;i<20;i++)
		printf("%c", a[i]);

	printf(", %c\n", first);

	return 0;
}
