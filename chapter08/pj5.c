#include <stdio.h>

#define NUM_RATES ((int)(sizeof(value) / sizeof(value[0])))
#define INITIAL_BALANCE 100.00

int main(void)
{
	int i, low_rate, num_years, year;
	double value[5];

	printf("Enter yearly interest rate: ");
	scanf("%d", &low_rate);
	printf("Enter number of years: ");
	scanf("%d", &num_years);

	printf("\nYears");
	for (i=0; i < NUM_RATES; i++){
		printf("%6d%%", low_rate + i);
		value[i] = INITIAL_BALANCE;
	}

	printf("\n");
	//printf("value[0] is:%f ", value[0]);
	//printf("value[1] is:%f ", value[0]);
	//printf("value[2] is:%f ", value[0]);
	//printf("value[3] is:%f ", value[0]);
	//printf("value[4] is:%f ", value[0]);
	for (year = 1; year <= num_years; year++){
		printf("%3d   ", year);
		for (i =0; i < NUM_RATES; i++){
			int k;
			for(k=0; k<12;k++){
				value[i] = value[i] + (low_rate + i) / 100.0 / 12 * value[i];
			}
			printf("%7.2f", value[i]);
		}
		printf("\n");
	}
	return 0;
}
