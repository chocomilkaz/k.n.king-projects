#include <stdio.h>

int main(void)
{
	int i, end, r, j;
	char a[30] = {0}, ch;

	printf("Enter a sentence: ");
	for(i = 0; (ch=getchar())!='.' && ch!='?' && ch!='!'; i++)
		a[i] = ch;

	end = ch;

	for(;i>0;i--){

		for(r = 0; a[i]!=' ' && i >= 0; i--, r++)
			;
		for(j=i+1; r>0; r--, j++)
			printf("%c", a[j]);
		if(i>0)
			printf(" ");
	}

				
	printf("%c\n", end);
	return 0;
}
