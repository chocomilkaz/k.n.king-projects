#include <stdio.h>

int main(void)
{
	int a[5][5]={0};
	int r, c;
	float r_total=0, c_total=0;

	for(r=0; r<5; r++){
		printf("Enter Student %d's score: ", r+1);
		for (c=0; c<5; c++){
			scanf("%d", &a[r][c]);
		}
	}

	printf("Student totals---------- \n");
	for (r =0 ; r < 5; r++){
		for (c=0; c < 5; c++){
			r_total += a[r][c];
		}
		printf("Student %d's total: %.0f. Average score is %.2f\n", r+1, r_total, r_total/5);
		r_total = 0;
	}
	printf("\n");

	printf("Quiz totals------------ \n ");
	for (c =0 ; c < 5; c++){
		for (r=0; r < 5; r++){
			c_total += a[r][c];
		}
		printf("Quiz %d's total: %.0f. Average score is %.2f\n", c+1, c_total, c_total/5);
		c_total = 0;
	}
	printf("\n");

	return 0;
}
