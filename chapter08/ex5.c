#include <stdio.h>

int main()
{
	int fib[40] = {0,1}, i;

	for (i =2; i < 40; i++){
		fib[i] = fib[i-2]+fib[i-1];
	}

	for (i=0; i < 40; i++){
		printf("fib [%.2d] is : %10d\n", i, fib[i]);
	}

	return 0;
}

