#include <stdio.h>
#include <stdbool.h>

int main(void)
{
	int digit_seen[10] = {0};
	int digit_repeated[10] = {0};
	int digit,i;
	long n;

	printf("Enter a number: ");
	scanf("%ld", &n);

	while (n > 0){
		digit = n % 10;
		if (digit_seen[digit]){
			digit_repeated[digit] = digit;
			n /= 10;
		}
		else{
			digit_seen[digit] = 1;
			n /= 10;
		}
	}

	printf("Repeated digit(s):");

	for(i=0; i <10;i++){
		if(digit_repeated[i])
			printf("%d ", digit_repeated[i]);
	}

	printf("\n");
	return 0;


}
