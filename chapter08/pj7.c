#include <stdio.h>

int main(void)
{
	int a[5][5]={0};
	int r, c, r_total=0, c_total=0;

	for(r=0; r<5; r++){
		printf("Enter row %d: ", r+1);
		for (c=0; c<5; c++){
			scanf("%d", &a[r][c]);
		}
	}

	printf("Row totals: ");
	for (r =0 ; r < 5; r++){
		for (c=0; c < 5; c++){
			r_total += a[r][c];
		}
		printf("%d ", r_total);
		r_total = 0;
	}
	printf("\n");

	printf("Column totals: ");
	for (c =0 ; c < 5; c++){
		for (r=0; r < 5; r++){
			c_total += a[r][c];
		}
		printf("%d ", c_total);
		c_total = 0;
	}
	printf("\n");

	return 0;
}
