#include <stdio.h>
#include <ctype.h>

int main(void)
{
	printf("Enter a word: ");

	int a[26] = {1,3,3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
	int score = 0, ch;

	while((ch=toupper(getchar()))!='\n')
		score += a[ch-'A'];

	printf("Scrabble score is: %d\n", score);

	return 0;
}

