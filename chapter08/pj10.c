#include <stdio.h>

int main(void)
{
	int tr1 = 60*8;
	int tr2 = 60*9+43;
	int tr3 = 60*11+19;
	int tr4 = 60*12+47;
	int tr5 = 60*14;
	int tr6 = 60*15+45;
	int tr7 = 60*19;
	int tr8 = 60*21+45;

	int tr1a = 60*10+16;
	int tr2a = 60*11+52;
	int tr3a = 60*13+31;
	int tr4a = 60*15+0;
	int tr5a = 60*16+8;
	int tr6a = 60*17+55;
	int tr7a = 60*21+20;
	int tr8a = 60*23+58;

	int inhour, inminute, input, r, c;
	int a[2][8] = {{tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8}, {tr1a, tr2a, tr3a, tr4a, tr5a, tr6a, tr7a, tr8a }};

	
	printf("Enter a 24-hour time : ");
	scanf("%d:%d", &inhour, &inminute);
	input = 60*inhour+inminute;

	for(r=0, c=0; input > a[r][c]; c++){
		;
	}
	
		
	printf("The closest train leaves at : %.2d:%.2d", a[r][c]/60, a[r][c]%60);
	printf(" and arrives at %.2d:%.2d\n", a[r+1][c]/60, a[r+1][c]%60);


	/*
	if (input < tr1)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr1/60, tr1%60, tr1a/60, tr1a%60);
	else if (input < tr2)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr2/60, tr2%60, tr2a/60, tr2a%60);
	else if (input < tr3)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr3/60, tr3%60, tr3a/60, tr3a%60);
	else if (input < tr4)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr4/60, tr4%60, tr4a/60, tr4a%60);
	else if (input < tr5)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr5/60, tr5%60, tr5a/60, tr5a%60);
	else if (input < tr6)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr6/60, tr6%60, tr6a/60, tr6a%60);
	else if (input < tr7)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr7/60, tr7%60, tr7a/60, tr7a%60);
	else if (input < tr8)
		printf("The closest train leaves at : %.2d:%.2d and arrives at %.2d:%.2d", tr8/60, tr8%60, tr8a/60, tr8a%60);

		*/

	return 0;

}

