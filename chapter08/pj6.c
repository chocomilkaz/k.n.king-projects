#include <stdio.h>
#include <ctype.h>

#define AL (int)(sizeof(a)/sizeof(a[0]))
#define N 250

int main(void)
{
	int i, ch;
	int a[N]={0};

	printf("Enter message: ");
	for(i=0; (ch=getchar())!='\n' ;i++){
		if(i > 249){
			printf("too long!!");
			return 0;
		}
	
		a[i] = ch;
	}

	for(i=0; i<AL; i++)
		a[i] = toupper(a[i]);

	for (i=0; i < AL; i++)
		printf("%c", a[i]);
	printf("\n");

	for(i=0; i<AL; i++){
		switch(a[i]){
			case 'A':
				a[i] = '4';
				break;
			case 'B':
				a[i] = '8';
				break;
			case 'E':
				a[i] = '3';
				break;
			case 'I':
				a[i] = '1';
				break;
			case 'O':
				a[i] = '0';
				break;
			case 'S':
				a[i] = '5';
				break;
			default :
				break;
		}
	}
	for (i=0; i < AL; i++)
		printf("%c", a[i]);
	printf("\n");	
	return 0;
}
