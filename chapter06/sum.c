#include <stdio.h>

int main(void)
{
	int n, sum = 0;

	printf("this program sums a series of integers.\n");
	printf("Enter integers (0 to terminate): ");

	scanf("%d", &n);
	while (n){
		sum += n;
		scanf("%d", &n);
	}
	printf(" the sum is : %d\n", sum);

	return 0;
		
}
