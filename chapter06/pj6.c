#include <stdio.h>

int main(void)
{
	int usr_input, i;

	printf("enter a number: ");
	scanf("%d", &usr_input);

	for(i=2; i*i < usr_input;i+=2){
		printf("%d\n", i*i);
	}

	return 0;		
}
