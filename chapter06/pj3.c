#include <stdio.h>

int main(void)
{
	int i1, i2, rem;
	int o1, o2;

	printf("enter fraction: ");
	scanf("%d/%d", &i1, &i2);

	o1 = i1;
	o2 = i2;
	for(;;){
		if(i2==0)
			break;
		else
			rem = i1 % i2;
			i1 = i2;
			i2 = rem;
	}

	printf("In lowest terms: %d/%d\n", o1/i1, o2/i1);

	return 0;
}
