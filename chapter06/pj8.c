#include <stdio.h>

int main(void)
{
	int days, st_day, i, x;
	for (;;){
		printf("Enter number of days in month: ");
		scanf("%d", &days);
		if(days <= 0 || days > 31)
			printf("Enter between 1-31");
		else
			break;
	}
	for(;;){
		printf("Enter starting day of the week : (1=sun, 7=sat)");
		scanf("%d", &st_day);
		if (st_day <= 0 || st_day >7)
			printf("Enter between 1-7");
		else
			break;
	}

	printf("Sun Mon Tue Web Thu Fri Sat\n");

	for(x=1; x < st_day; x++)
		printf("    ");

	for(i=1; i <= days; i++){
		printf("%3d ", i);
		if ((st_day -1 + i)% 7==0)
			printf("\n");
	}
	printf("\n");
	return 0;
		


	

	
}
