#include <stdio.h>

int main(void)
{
	int i1, i2, rem;

	printf("enter two integers: ");
	scanf("%d%d", &i1, &i2);

	for(;;){
		if(i2==0)
			break;
		else
			rem = i1 % i2;
			i1 = i2;
			i2 = rem;
	}

	printf("Greatest common divisor: %d\n", i1);

	return 0;
}
