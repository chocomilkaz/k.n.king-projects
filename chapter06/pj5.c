#include <stdio.h>

int main(void)
{
	int input;

	printf("Enter a integer number: ");
	scanf("%d", &input);

	printf("The reversal is : ");
	do{
		printf("%d", input % 10);
		input = input /  10;
	}while(input);
	printf("\n");

}
