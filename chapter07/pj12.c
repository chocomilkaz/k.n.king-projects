#include <stdio.h>

int main(void)
{
	printf("Enter an expression: ");

	float second, result;
	int ope;

	scanf("%f", &result);
	ope = getchar();

	do{
		scanf("%f", &second);
		switch(ope){
			case '+':
				result += second;
				break;
			case '-':
				result -= second;
				break;
			case '*':
				result *= second;
				break;
			case '/':
				result /= second;
				break;
		}
		ope = getchar();
	}while(ope != '\n');
	printf("Value of expression: %.2f\n", result);
	return 0;
}
