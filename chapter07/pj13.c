#include <stdio.h>

int main(void)
{
	printf("Enter a sentence: ");

	int ch, count_total=0;
	float average, word_count, word_length, word_total=0;
	ch = getchar();
	for (word_count=0; ch!='\n'; word_count++){
		for(word_length=0; ch!='\n' && ch!=' ';word_length++){
			ch=getchar();
		}

		word_total += word_length;

		if (ch == ' ')
			ch=getchar();
	
	}
	
	printf("Average word length: %.2f\n", word_total / word_count);
	printf("Word total = %.0f, word_count= %.0f\n", word_total, word_count);
	return 0;		
}
