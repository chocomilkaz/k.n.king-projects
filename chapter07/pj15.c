#include <stdio.h>

int main(void)
{
	int input, result, origin;

	printf("Enter a positive number: ");
	scanf("%d", &input);
	origin = input;

	for(result = input ;input > 1 ; input--){
		result *= (input-1);
	}

	printf("Factorial of %d: %d", origin, result);
	return 0;
}

