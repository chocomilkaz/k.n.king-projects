#include <stdio.h>
#include <ctype.h>

int main(void)
{
	printf("Enter a sentence: \n");
	int i, test;

	for(i=0; (test=getchar())!='\n';){
		if (toupper(test) == 'A' ||toupper(test) == 'E' ||toupper(test) == 'I' ||toupper(test) == 'O' ||toupper(test) == 'U') 
			i++;
	}
			
		
	printf("Your sentence contains %d vowels.\n", i);

	return 0;
		
}
