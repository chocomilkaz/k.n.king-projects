#include <stdio.h>
#include <ctype.h>

int main(void)
{
	int hour, minute, set;

	printf("Enter a 12-hour time: ");
	scanf("%d:%d", &hour, &minute);
	while((set=getchar())==' ')
		;

	if('P'==toupper(set)){
		if (hour ==12)
			printf("Equivalent 24-hour time: %.2d:%.2d\n", hour, minute);
		else
			printf("Equivalent 24-hour time: %.2d:%.2d\n", hour+12, minute);
	}
	else{
		if (hour+12 ==24)
			printf("Equivalent 24-hour time: %.2d:%.2d\n", hour-12, minute);
		else
			printf("Equivalent 24-hour time: %.2d:%.2d\n", hour, minute);
	}

	return 0;
}
