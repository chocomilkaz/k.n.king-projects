#include <stdio.h>
#include <math.h>

int main(void)
{
	double input, div= 1, ave;
	int count=0;
	printf("Enter a positive number: ");
	scanf("%lf", &input);
	
	do{
		ave = (div + input/div) / 2;
		if(fabs(div - ave) < 0.00000001 * div)
			break;
		div = ave;
		count++;
		
	}while(1);

	printf("Square root: %f, iteration = %d\n", div, count);
	
	return 0;
}

